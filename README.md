<p align="center"><a href="https://medium.com/@notrab/getting-started-with-create-react-app-redux-react-router-redux-thunk-d6a19259f71f"><img src="https://i.imgur.com/PATsTx2.png" title="View tutorial" alt="React, React Router, Redux and Redux Thunk" width="900"></a></p>

* Tutorial: [Getting started with create-react-app, Redux, React Router & Redux Thunk](https://medium.com/@notrab/getting-started-with-create-react-app-redux-react-router-redux-thunk-d6a19259f71f)
* [Demo](https://create-react-app-redux.now.sh) 🙌

## Installation

```bash
git clone git@gitlab.com:razorf80/product-selection.git
cd product-selection
yarn
```

## Get started

```bash
yarn start
```

This boilerplate is built using [product-selection](https://gitlab.com/razorf80/product-selection) so you will want to read the User Guide for more goodies.
