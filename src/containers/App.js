import React from 'react';
import { Route } from 'react-router-dom';
import Product from './Product/index';

const App = () => (
  <div>
    <main>
      <Route path="/products">
        <Route path="/products/:page/:row" defaultParams={{ page: '1', row: '8' }} component={Product} />
      </Route>
    </main>
  </div>
);

export default App;
