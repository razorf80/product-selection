import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Product from '../../component/Product';
import * as actions from '../../actions/product';

const mapStateToProps = state => ({
  products: state.products.products,
  location: state.router.location,
});

const mapDispatchToProps = {
  getProducts: actions.getProducts,
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Product));
