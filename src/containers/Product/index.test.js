/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import Product from './index';
import ProductHeader from '../../component/Product/ProductHeader/index';
import ProductGrid from '../../component/Product/ProductGrid/index';
import ProductFooter from '../../component/Product/ProductFooter/index';

describe('<Product/>', () => {
  const wrap = (props = {}) => shallow(<Product.WrappedComponent {...props} />).dive();

  it('should have all the components', () => {
    const initial = {
      products: [],
      match: {
        params: {
          row: '8',
          page: '1',
        },
      },
      getProducts: () => new Promise((resolve) => {
        resolve(true);
      }),
    };

    const wrapper = wrap(initial);

    const header = wrapper.find(ProductHeader);
    expect(header.length).toEqual(1);

    const grid = wrapper.find(ProductGrid);
    expect(grid.length).toEqual(1);

    const footer = wrapper.find(ProductFooter);
    expect(footer.length).toEqual(1);
  });
});
