import * as types from '../constants/productTypes';

const initialState = {
  products: [],
  isLoading: false,
};

export default function products(state = initialState, action) {
  switch (action.type) {
    case types.GET_PRODUCTS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case types.GET_PRODUCTS_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case types.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.payload,
        isLoading: false,
      };

    default:
      return state;
  }
}
