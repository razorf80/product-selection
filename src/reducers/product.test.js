/* global it, describe, expect */
import products from './products';
import * as types from '../constants/productTypes';

describe('products reducer', () => {
  it('should return the initial state', () => {
    expect(products(undefined, {})).toEqual(
      {
        products: [],
        isLoading: false,
      },
    );
  });

  it('should handle GET_PRODUCTS_REQUEST', () => {
    expect(
      products([], {
        type: types.GET_PRODUCTS_REQUEST,
      }),
    ).toEqual(
      {
        isLoading: true,
      },
    );
  });

  it('should handle GET_PRODUCTS_ERROR', () => {
    expect(
      products({}, {
        type: types.GET_PRODUCTS_ERROR,
      }),
    ).toEqual(
      {
        isLoading: false,
      },
    );
  });

  it('should handle GET_PRODUCTS_SUCCESS', () => {
    const mockProduct = [
      {
        id: 1,
        price: '$87.68',
        product_name: 'Amitriptyline Hydrochloride',
        description: 'synergize efficient metrics',
        product_image: 'http://dummyimage.com/307x328.bmp/ff4444/ffffff',
      },
      {
        id: 2,
        price: '$58.53',
        product_name: 'Zicam',
        description: 'repurpose world-class metrics',
        product_image: 'http://dummyimage.com/345x342.jpg/dddddd/000000',
      },
    ];

    expect(
      products(
        undefined,
        {
          type: types.GET_PRODUCTS_SUCCESS,
          payload: mockProduct,
        },
      ),
    ).toEqual(
      {
        products: mockProduct,
        isLoading: false,
      },
    );
  });
});
