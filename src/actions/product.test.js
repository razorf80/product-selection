/* global test, expect */
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getProducts } from './product';
import * as types from '../constants/productTypes';

// Configuring a mockStore
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const MockAdapter = require('axios-mock-adapter');

// This sets the mock adapter on the default instance
const axios = require('axios');

const mock = new MockAdapter(axios);

test('test getSomeData', () => {
  const store = mockStore({});
  const products = [
    {
      id: 1,
      price: '$87.68',
      product_name: 'Amitriptyline Hydrochloride',
      description: 'synergize efficient metrics',
      product_image: 'http://dummyimage.com/307x328.bmp/ff4444/ffffff',
    },
  ];

  mock
    .onGet('https://whitechdevs.github.io/reactjs-test/products.json')
    .reply(
      200,
      products,
    );


  const expectedActions = [
    { type: types.GET_PRODUCTS_REQUEST },
    { type: types.GET_PRODUCTS_SUCCESS, payload: products },
  ];

  store.dispatch(getProducts()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('test errorData', () => {
  const store = mockStore({});

  mock
    .onGet('https://whitechdevs.github.io/reactjs-test/products.json')
    .reply(500);


  const expectedActions = [
    { type: types.GET_PRODUCTS_REQUEST },
    { type: types.GET_PRODUCTS_ERROR },
  ];

  store.dispatch(getProducts()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});
