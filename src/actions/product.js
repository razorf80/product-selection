import axios from 'axios';
import * as types from '../constants/productTypes';

export const getProducts = () => (dispatch) => {
  dispatch({
    type: types.GET_PRODUCTS_REQUEST,
  });

  return axios.get('https://whitechdevs.github.io/reactjs-test/products.json')
    .then((response) => {
      dispatch({
        type: types.GET_PRODUCTS_SUCCESS,
        payload: response.data,
      });
    })
    .catch(() => {
      dispatch({
        type: types.GET_PRODUCTS_ERROR,
      });
    });
};
