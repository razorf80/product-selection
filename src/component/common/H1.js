import styled from 'styled-components';

export const H1 = styled.h1`
  color: #676767;
  font-size: 16px;
`;
