import styled from 'styled-components';

export const H2 = styled.h2`
  color: #BCBCBC;
  font-size: 14px;
`;

export const H2BlackBold = styled(H2)`
  color: #000000;
  font-weight: bold;
`;

export const H2GreyBold = styled(H2)`
  color: #676767;
  font-weight: bold;
`;
