/* global describe, it, expect */
import React from 'react';
import { Select, MenuItem } from '@material-ui/core';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import RowSelect from './index';

describe('<RowSelect/>', () => {
  const wrap = (props = {}) => shallow(<RowSelect {...props} />).dive();

  it('shoud behave correctly given values', () => {
    const values = ['8', '16', '32'];
    const selected = '8';
    const changeFn = sinon.spy();
    const wrapper = wrap({ values, selected, onChange: changeFn });

    const select = wrapper.find(Select);
    expect(select.length).toEqual(1);
    expect(select.prop('value')).toEqual('8');

    select.prop('onChange')({ target: { value: 8 } });
    expect(changeFn.calledOnce).toEqual(true);

    const menuItem = wrapper.find(MenuItem);
    expect(menuItem.length).toEqual(3);
  });

  it('should not crash on empty values', () => {
    const wrapper = wrap({});

    const menuItem = wrapper.find(MenuItem);
    expect(menuItem).toBe.undefined;
  });
});
