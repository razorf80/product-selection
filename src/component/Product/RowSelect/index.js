import React from 'react';
import PropTypes from 'prop-types';
import { Select, MenuItem, FormControl } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit,
  },
  form: {
    display: 'inline-block',
    float: 'right',
  },
});

const RowSelect = ({
  classes, onChange, values, selected,
}) => (
  <form autoComplete="off" className={classes.form}>
    <FormControl className={classes.formControl}>
      <Select
        value={selected}
        onChange={(event) => { onChange(event.target.value); }}
      >
        {
						values.map(value => (
  <MenuItem key={value} value={value}>{value}</MenuItem>
						))
					}
      </Select>
    </FormControl>
  </form>
);

RowSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  values: PropTypes.array,
  selected: PropTypes.string,
  onChange: PropTypes.func,
};

RowSelect.defaultProps = {
  values: [],
  selected: '',
  onChange: null,
};

export default withStyles(styles)(RowSelect);
