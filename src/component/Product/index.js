import React, { PureComponent } from 'react';
import ProductGrid from './ProductGrid';
import { Wrapper } from '../common/Wrapper';
import ProductHeader from './ProductHeader';
import ProductFooter from './ProductFooter';

export default class Product extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      rowValues: ['8', '16', '24'],
      maxPage: 0,
      currentPage: 0,
    };
  }

  componentDidMount() {
    const { getProducts } = this.props;
    getProducts();
  }

  componentWillReceiveProps(nextProps) {
    this.getCurrentPage(nextProps);
  }

  getCurrentPage = (nextProps) => {
    const { page, row } = nextProps.match.params;
    const { products } = nextProps;
    const startIndex = (page - 1) * row;
    const endIndex = startIndex + parseInt(row, 0);
    const maxPage = Math.ceil(products.length / row);

    this.setState({
      productList: products.slice(startIndex, endIndex),
      maxPage: parseInt(maxPage, 10),
      currentPage: parseInt(page, 10),
    });
  }

  handleRowChange = (value) => {
    const { match } = this.props;
    this.handleRouteChange(match.params.page, value);
  }

  handlePageChange = (value) => {
    const { match } = this.props;
    this.handleRouteChange(value, match.params.row);
  }

  handleRouteChange = (page, row) => {
    const { history } = this.props;
    const newRoute = `/products/${page}/${row}`;
    history.push(newRoute);
  }

  render() {
    const { products, match } = this.props;
    const {
      rowValues, currentPage, maxPage, productList,
    } = this.state;
    return (
      <Wrapper>
        <ProductHeader
          products={products}
          row={match.params.row}
          rowOptions={rowValues}
          onChange={this.handleRowChange}
        />
        <ProductGrid
          products={productList}
        />
        <ProductFooter
          page={currentPage}
          maxPage={maxPage}
          onClick={this.handlePageChange}
        />
      </Wrapper>
    );
  }
}
