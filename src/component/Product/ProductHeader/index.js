import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { H1 } from '../../common/H1';
import { H2 } from '../../common/H2';
import RowSelect from '../RowSelect';

const SubWrapper = styled.div`
  border-bottom: 1px solid #E8E8E8;
  width: 100%;
  float: right;
`;
export const RightRowSelect = styled(RowSelect)`
  right: 0px;
`;

export const H2Inline = styled(H2)`
  display: inline-block;
`;

const ProductHeader = ({
  products, row, rowOptions, onChange,
}) => (
  <div>
    <H1>All Products</H1>
    <SubWrapper>
      <H2Inline>
        {products ? products.length : 0}
        {' '}
      Products
      </H2Inline>
      <RightRowSelect
        selected={row}
        values={rowOptions}
        onChange={onChange}
      />
    </SubWrapper>
  </div>
);

ProductHeader.propTypes = {
  products: PropTypes.array,
  row: PropTypes.string,
  rowOptions: PropTypes.array,
  onChange: PropTypes.func,
};

ProductHeader.defaultProps = {
  products: [],
  row: '',
  rowOptions: [],
  onChange: null,
};

export default ProductHeader;
