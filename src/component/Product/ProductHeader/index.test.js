/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import ProductHeader from './index';
import { H2Inline } from './index';

describe('<ProductHeader/>', () => {
  const wrap = (props = {}) => shallow(<ProductHeader {...props} />);

  it('should not crash on empty values', () => {
    const wrapper = wrap({});

    // check title is correct
    const title = wrapper.find(H2Inline).shallow();
    expect(title.render().text()).toBe('0 Products');
  });

  it('should return correct values', () => {
    const products = [1, 2];

    const wrapper = wrap({ products });

    // check title is correct
    const title = wrapper.find(H2Inline);
    expect(title.render().text()).toBe('2 Products');
  });
});
