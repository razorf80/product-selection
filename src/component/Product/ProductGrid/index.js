import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ProductItem from '../ProductItem/index';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

const ProductGrid = (props) => {
  const { classes, products } = props;
  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        {
            products.map(product => (
              <Grid key={product.id} item xs={12} sm={6} md={4} lg={3}>
                <ProductItem product={product} />
              </Grid>
            ))
          }
      </Grid>
    </div>
  );
};

ProductGrid.propTypes = {
  classes: PropTypes.object.isRequired,
  products: PropTypes.array,
};

ProductGrid.defaultProps = {
  products: [],
};

export default withStyles(styles)(ProductGrid);
