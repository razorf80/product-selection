/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import ProductGrid from './index';
import ProductItem from '../ProductItem/index';

describe('<ProductGrid/>', () => {
  const wrap = (props = {}) => shallow(<ProductGrid {...props} />).dive();

  it('should not crash on empty values', () => {
    const wrapper = wrap({});

    // check item length
    const item = wrapper.find(ProductItem);
    expect(item.length).toEqual(0);
  });

  it('should return correct values', () => {
    const products = [{ id: 1 }, { id: 2 }];

    const wrapper = wrap({ products });

    // check item length
    const item = wrapper.find(ProductItem);
    expect(item.length).toEqual(2);
  });
});
