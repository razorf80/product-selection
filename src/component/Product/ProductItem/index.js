import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { H2, H2BlackBold, H2GreyBold } from '../../common/H2';

const Wrapper = styled.section`
  background: white;
  border: 1px solid #E8E8E8;
`;

const ImageBox = styled.div`
  padding: 10px;
`;
const Image = styled.img`
  width: 180px;
  height: 180px;
  display: block;
  margin-left: auto;
  margin-right: auto;
`;

const InfoBox = styled.div`
  border-top: 1px solid #E8E8E8;
  padding: 10px;
  line-height: 1.2;
`;

const ProductItem = ({ product }) => (
  <Wrapper>
    <ImageBox>
      <Image src={product.product_image} />
    </ImageBox>
    <InfoBox>
      <div>
        <H2GreyBold>{product.product_name}</H2GreyBold>
      </div>
      <div>
        <H2>{product.description}</H2>
      </div>
      <div>
        <H2BlackBold>{product.price}</H2BlackBold>
      </div>
    </InfoBox>
  </Wrapper>
);

ProductItem.propTypes = {
  product: PropTypes.object,
};

ProductItem.defaultProps = {
  product: {
    product_image: '',
    product_name: '',
    description: '',
    price: '',
  },
};

export default ProductItem;
