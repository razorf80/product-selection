/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import ProductItem from './index';

describe('<ProductItem/>', () => {
  const wrap = (props = {}) => shallow(<ProductItem {...props} />).dive();

  it('should not crash on empty values', () => {
    const product = {
      product_image: 'a',
      product_name: 'b',
      description: 'c',
      price: 'd',
    };

    const wrapper = wrap({ product });
    const html = wrapper.html();
    expect(html).toContain('a');
    expect(html).toContain('b');
    expect(html).toContain('c');
    expect(html).toContain('d');
  });
});
