import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const Wrapper = styled.div`
  display: inline-block;
  width: 100%;
`;

const SubWrapper = styled.div`
  float:right;
`;
const active = css`
  background-color: #FFFFFF;
  border-bottom: 5px solid black;
`;
const disabled = css`
  color: #BCBCBC;
  &:hover{
    background-color: transparent;
  }
`;
export const ButtonLink = styled.button`
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  &:hover{
    background-color: #FFFFFF;
  }
  ${props => props.isActive && active}
  ${props => props.isDisabled && disabled}
`;
const getLink = (
  key,
  page,
  label,
  onClick,
  isActive,
  isDisabled,
) => (
  <ButtonLink
    key={key}
    id={key}
    isActive={isActive}
    isDisabled={isDisabled}
    onClick={
      !isDisabled
        ? () => onClick(page)
        : ''
    }
  >
    {label}
  </ButtonLink>
);

const ProductFooter = ({ page, maxPage, onClick }) => {
  const startPage = page === maxPage
    ? page - 2
    : page - 1;

  const endPage = Math.min(
    maxPage,
    startPage > 0
      ? page + 1
      : page + 2,
  );

  let index = Math.max(1, startPage);
  const pages = [];

  while (index <= endPage) {
    const isActive = index === page;
    pages.push(getLink(
      `page-${index}`,
      index,
      index,
      onClick,
      isActive,
      false,
    ));
    index += 1;
  }

  const previous = getLink(
    'previousPage',
    page - 1,
    '< Previous',
    onClick,
    false,
    page === 1,
  );

  const next = getLink(
    'nextPage',
    page + 1,
    'Next >',
    onClick,
    false,
    page === maxPage,
  );

  return (
    <Wrapper>
      <SubWrapper>
        {previous}
        {pages}
        {next}
      </SubWrapper>
    </Wrapper>
  );
};

ProductFooter.propType = {
  page: PropTypes.number,
  maxPage: PropTypes.number,
  onClick: PropTypes.func,
};

ProductFooter.defaultProps = {
  page: 0,
  maxPage: 0,
  onClick: null,
};

export default ProductFooter;
