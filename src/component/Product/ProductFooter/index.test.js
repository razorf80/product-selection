/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import ProductFooter from './index';
import { ButtonLink } from './index';

describe('<ProductFooter/>', () => {
  const wrap = (props = {}) => shallow(<ProductFooter {...props} />).dive();

  it('shoudl work with some values', () => {
    const changeFn = sinon.spy();
    const page = 1;
    const maxPage = 10;

    const wrapper = wrap({ page, maxPage, onClick: changeFn });

    const links = wrapper.find(ButtonLink);
    expect(links.length).toEqual(5);

    // check previous
    const previous = links.at(0);
    expect(previous.prop('isDisabled')).toEqual(true);
    expect(previous.prop('onClick')).toEqual('');

    // check page 1 is selected
    const page1 = links.at(1);
    expect(page1.prop('isActive')).toEqual(true);
    page1.prop('onClick')();
    expect(changeFn.calledWith(1)).toEqual(true);

    // check next
    const next = links.at(4);
    expect(next.prop('isDisabled')).toEqual(false);
    next.prop('onClick')();
    expect(changeFn.calledWith(2)).toEqual(true);

    // check label
    const html = wrapper.html();
    expect(html).toContain('&lt; Previous');
    expect(html).toContain('1');
    expect(html).toContain('2');
    expect(html).toContain('3');
    expect(html).toContain('Next &gt');
  });
});
