import { injectGlobal } from 'styled-components';

injectGlobal`
@import url('https://fonts.googleapis.com/css?family=Raleway');

html,
body {
  margin: 0;
  font-family: 'Raleway', sans-serif;
}
`;
